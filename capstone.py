from abc import ABC, abstractclassmethod
class Person(ABC):
	@abstractclassmethod
	def getFullName(self, firstName, lastName):
		pass
	def addRequest(self):
		pass
	def checkRequest(self):
		pass
	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
# SETTERS
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department
# GETTERS
	def get_firstName(self):
		print(f"First Name: {self._firstName}")

	def get_lastName(self):
		print(f"Last Name: {self._lastName}")

	def get_email(self):
		print(f"Email address: {self._email}")

	def get_department(self):
		print(f"Department: {self._department}")

	def getFullName(self):
		print(f"{self._firstName} {self._lastName}")
# OTHER METHODS
	def checkRequest(self):
		print(f"{self._firstName}'s request is to help reach the companies goals and objectives")

	def addUser(self):
		print("User has been added")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

	def addRequest(self):
		print(f"Request has been added")

class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
# SETTERS
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department
# GETTERS
	def get_firstName(self):
		print(f"First Name: {self._firstName}")

	def get_lastName(self):
		print(f"Last Name: {self._lastName}")

	def get_email(self):
		print(f"Email address: {self._email}")

	def get_department(self):
		print(f"Department: {self._department}")

	def getFullName(self):
		print(f"{self._firstName} {self._lastName}")
# OTHER METHODS
	def checkRequest(self):
		print(f"{self._firstName}'s request is to teach the new employees")

	def addUser(self):
		print("User has been added")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

	def addMember(self, firstName):
		print(f"{self._firstName} was added to the list of Team Lead")

	def addRequest(self):
		print(f"Request has been added")

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
# SETTERS
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department
# GETTERS
	def get_firstName(self):
		print(f"First Name: {self._firstName}")

	def get_lastName(self):
		print(f"Last Name: {self._lastName}")

	def get_email(self):
		print(f"Email address: {self._email}")

	def get_department(self):
		print(f"Department: {self._department}")

	def getFullName(self):
		print(f"{self._firstName} {self._lastName}")
# OTHER METHODS
	def checkRequest(self):
		print(f"{self._firstName}'s request is to hire new employees")

	def addUser(self):
		print("User has been added")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

	def addUser(self):
		print(f"User has been added")

class Request():
	def __init__(self, name, requester, dateRequested):
		super().__init__()

		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "ACTIVE"

	def updateRequest(self):
		print(f"{self._name} Request has been updated!")

	def closeRequest(self):
		print(f"Request {self._name} has been closed!")

	def cancelRequest(self):
		print(f"Request {self._name} has been cancelled!")

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamlead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamlead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

employee1.getFullName()
admin1.getFullName()
teamlead1.getFullName()
employee2.login()
employee2.addRequest()
employee2.logout()
req2.closeRequest()

# admin1.checkRequest()

employee3.getFullName()
employee4.getFullName()
req2.closeRequest()